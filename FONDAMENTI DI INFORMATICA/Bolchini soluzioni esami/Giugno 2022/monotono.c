#include <stdio.h>
#define BASE 10
int ncifre(int n){      /*sottopogramma per calcolare il numero di cifre, visto a lezione pi� volte*/
	int digit;
	
	digit=0;

	while(n>0){
		n=n/10;
		digit++;
	}
	return digit;
}


int monotono(int n, int *nc){       /*ricorda che il parametro da trasmettere va passato come indirizzo*/
	int tmp, cifra, cifrapre, numcifre;
	int ism;

	ism=1;
	tmp=n;   /*setto la variabile temporanea uguale a il numero in ingresso*/
	cifrapre=n % BASE;      /*nella variabile cifrapre salvo l'ultima cifra del numero in ingresso*/
	tmp= n / BASE;           /*riduco il numero*/
	while(tmp>0 && ism==1){     /*il ciclo termina finch� non analizzo tutto il numero in ingresso o fino a quando non incorro in errore*/
		cifra=tmp % BASE;       /*nella variabile cifra salvo la penultima cifra del numero in ingresso (ci� possibile grazie alla riduzione precedente)*/
		if(cifra<=cifrapre)     /*se la penultima cifra � minore-uguale dell'ultima, errore, ism=0, esco dal ciclo e restituisco 0...*/
			ism=0;
		else
			cifrapre=cifra;     /*in caso contrario continuo ad analizzare il numero in ingresso, continuando a ridurlo e confrontare l'ultima e la penultima cifra*/
		tmp /= BASE;
	}
	numcifre=ncifre(n);     /*utilizzo il sottoprogramma sviluppato in precedenza per salvare nella variabile numcifre il numero di cifre del numero in ingresso*/
	*nc=numcifre;           /*trasmetto il numero di cifre ponendo l'indirizzo in ingresso pari a numcifre*/
	return ism;
}


