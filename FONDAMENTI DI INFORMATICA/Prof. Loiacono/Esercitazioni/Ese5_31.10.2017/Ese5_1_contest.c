/*

Scrivere un programma che chiede all'utente di inserire dele stringhe (senza spazi e di lunghezza al max 100).
Il programma termina non appena l'utente inserisce la stringa "stop", stampando il numero di stringhe inserite dall'utente
di lunghezza l (dove l è a sua volta inserito dall'utente DOPO l'acquisizione delle stringhe)

*/

#include <stdio.h>
#include <string.h>

#define N 100

int leggiNumeroInteroPositivo();

int main()
{
    int lunghezza;
    int lunghezze[N] = {0}; //tutti gli int dell'array inizializzati a 0

    char stringa[N], stop[] = "stop";

    printf("Inserisci una stringa senza spazi (stop per terminare):\n");
    do {
        scanf("%s%*c", stringa);
        if(strcmp(stringa,stop) != 0)
            lunghezze[strlen(stringa)]++;
    } while(strcmp(stringa,stop) != 0);

    lunghezza = leggiNumeroInteroPositivo();

    printf("Il numero di stringhe di lunghezza %d è %d\n", lunghezza, lunghezze[lunghezza]);

    return 0;
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Insersci la lunghezza: ");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig  || num > N); //cicla finchè non inseriamo un numero positivo e intero, e minore di N poichè la lunghezza massima della stringa è N

    return num;
}
