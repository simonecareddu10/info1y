/*

Scrivere un programma che legga una stringa e stampi a video se è palindroma

*/

#include <stdio.h>
#include <string.h>

#define N 1000

int main()
{

    int i, n;
    char stringa[N], stringa2[N];
    printf("Inserisci la stringa senza spazi: ");
    scanf("%s", stringa);

    n = strlen(stringa); //strlen(stringa) ritorna la lunghezza effettiva della stringa

    for(i = 0; i<n; i++)
    {
        stringa2[n-i-1] = stringa[i]; //mette nella stringa2 la stringa 1 invertita
    }

    if(strcmp(stringa,stringa2) == 0) //strcmp(stringa,stringa2) ritorna 0 se le due stringhe sono uguali
        printf("La stringa è palindroma\n");
    else
        printf("La stringa non è palindroma\n");

    return 0;
}
