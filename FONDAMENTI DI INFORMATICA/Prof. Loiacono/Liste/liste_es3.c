#include "liste_int.h"
#include <time.h>
#include <string.h>

lista int2list(int x);
int list2int(lista l);
lista somma(lista a, lista b);

int main()
{
  int a,b;
  lista la,lb,lsum;

  scanf("%d%*c",&a);
  scanf("%d%*c",&b);
  la = int2list(a);
  printf("la: ");
  stampa(la,1);
  lb = int2list(b);
  printf("lb: ");
  stampa(lb,1);
  lsum = somma(la,lb);
  printf("lsum: ");
  stampa(lsum,1);
  printf("%d + %d = %d\n",a,b,list2int(lsum));
  return 0;
}

lista int2list(int x)
{
  lista l = NULL;
  while (x>=10)
  {
    l = inserisci_in_coda(l,x%10,1);
    x = x/10;
  }
  return inserisci_in_coda(l,x,1);
}

int list2int(lista l)
{
  int x=0;
  lista inv = NULL;
  while (l!=NULL)
  {
    inv = inserisci_in_testa(inv,l->el);
    l = l->next;
  }

  while (inv!=NULL)
  {
    x = x*10 + inv->el;
    inv = inv->next;
  }
  return x;
}

lista somma(lista a, lista b)
{

  lista sum=NULL;
  int carry=0;
  while (a!=NULL && b!=NULL)
  {
    int ris = a->el + b->el + carry;
    sum = inserisci_in_coda(sum,ris%10,1);
    carry = ris/10;
    a = a->next;
    b = b->next;
  }

  if (a!=NULL)
  {
    while (a!=NULL)
    {
      int ris = a->el + carry;
      sum = inserisci_in_coda(sum,ris%10,1);
      carry = ris/10;
      a = a->next;
    }
  }
  else
  {
    while (b!=NULL)
    {
      int ris = b->el + carry;
      sum = inserisci_in_coda(sum,ris%10,1);
      carry = ris/10;
      b = b->next;
    }
  }

  if (carry>0)
    return inserisci_in_coda(sum,carry,1);
  else
    return sum;
}
