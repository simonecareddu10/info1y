#include <stdio.h>
#include <stdlib.h>

#define STAMPA "%c -> "
typedef char data;


struct nodo
{
    data el;
    struct nodo *next;
};

typedef struct nodo *lista;

// Tutte le seguenti funzioni hanno un parametro intero ric se uguale ad 1 viene
// eseguita la versione ricorsiva, altrimenti vieni eseguita quella iterativa

// Calcola e ritorna la lunghezza della lista
int lunghezza (lista l, int ric);

// Ricerca (il primo) elemento el nella lista l e ritorna il puntatore a
// quell'elemento (o NULL se l'elemento non è presente)
lista ricerca (lista l, data el, int ric);

// Inserisce un elemento el in testa alla lista l e ritorna un puntatore
// alla testa della lista modificata
lista inserisci_in_testa (lista l, data el);

// Inserisce un elemento el in coda alla lista l e ritorna un puntatore
// alla testa della lista modificata
lista inserisci_in_coda (lista l, data el, int ric);

// Rimuove l'elemento in testa alla lista l e ritorna un puntatore
// alla testa della lista modificata
lista rimuovi_in_testa (lista l);

// Rimuove l'elemento in coda alla lista l e ritorna un puntatore
// alla testa della lista modificata
lista rimuovi_in_coda (lista l, int ric);

// Rimuove (il primo) elemento el nella lista l (se presente) e ritorna un puntatore
// alla testa della lista modificata
lista rimuovi (lista l, data el, int ric);

// Stampa il contenuto della lista l
void stampa (lista l, int ric);
