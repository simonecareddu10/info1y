#include <stdio.h>

int leggiNumeroInteroPositivo();
int sommaDivisoriPropri(int num);
int amici(int a, int b);

int main()
{

    int num, sommaDiv, numAmico;

    num = leggiNumeroInteroPositivo();

    sommaDiv = sommaDivisoriPropri(num);

    if(num == sommaDiv)
        printf("Il numero %d è perfetto\n", num);
    else {
        printf("Il numero %d non è perfetto\n", num);
        numAmico = leggiNumeroInteroPositivo();
        if(amici(num, numAmico))
            printf("I numeri %d e %d sono amicabili\n", num, numAmico);
        else
            printf("I numeri %d e %d non sono amicabili\n", num, numAmico);
    }

    return 0;
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num;

    do {
        printf("Insersci un numero interno positivo: ");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while(num < 1 || num != num_orig); //cicla finchè non inseriamo un numero positivo e interno

    return num;
}

int sommaDivisoriPropri(int num) {

    int i, somma = 0;

    for(i=1; i<num-1; i++) //num-1 perchè non voglio che num venga diviso per sé stesso (per la definizione di numero perfetto)
        if(num%i == 0) //se il resto della divisione e 0, i è un divisore proprio di num
            somma = somma + i;

    return somma;
}

int amici(int a, int b) {
    return (sommaDivisoriPropri(a) == b && sommaDivisoriPropri(b) == a); //se i numeri sono amicabili ritorna 1, altrimenti 0;
}
