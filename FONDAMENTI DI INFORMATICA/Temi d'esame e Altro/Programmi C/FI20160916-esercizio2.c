/*
Un array di interi si definisce positivo se ciascuno dei suoi elementi è maggiore o uguale di 0, strettamente positivo se ciascuno dei suoi elementi è maggiore di 0. Ad esempio, {4,3,3,2,1} è strettamente positivo, mentre {4,5,0,2,1} è positivo, infine {4,5,-3,2,1} non è ne positivo ne strettamente positivo.

Implementare in C una funzione ricorsiva per determinare se un array è positivo o strettamente positivo. La funzione riceve in ingresso un array di interi e la
sua dimensione; ritorna 1 se l’array è strettamente positivo, 0 se l’array è positivo, -1 altrimenti.

Nota. Un array vuoto è strettamente positivo.


*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int positivo(int a[], int n);

int main() {
    int a[] = {1, 3, -1, 6};

    printf("%d\n", positivo(a, 4));
    return 0;
}

int positivo(int a[], int n) {

    int tmp;

    if(n<=0) //array vuoto
        return 1;

    tmp = positivo(a+1,n-1);
    if (tmp == -1 || a[0]<0)
        return -1;
    if ( (a[0]==0 && tmp>=0) || (a[0]>0 && tmp==0) )
        return 0;
    if (a[0]>0 && tmp==1)
        return 1;

}
