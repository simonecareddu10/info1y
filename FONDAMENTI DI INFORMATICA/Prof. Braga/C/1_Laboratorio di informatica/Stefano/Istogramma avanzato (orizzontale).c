#include <stdio.h>
#include <string.h>

#define N 10

int main(){
	
	int array[N] = {13, -3, 9, 7, 11, 8, 4, 1, 12, -2};
	int max=0;
	int min=0;
	char c='*';
	char space[]="     ";
	char zero[]="-----";
	char med[]="=====";
	max=array[0];
	min=array[0];
	int i;
	for(i=1; i<N; i++){
		if(array[i]>max){
			max=array[i];
		}
		if(array[i]<min){
			min=array[i];
		}
	}
	
	int media=0;
	for(i=0; i<N; i++){
		media+=array[i];
	}
	media/=N;
	
	for(; max!=-1; max--){
		printf("\n%5d", max);
		i=0;
		for(i=0; i<N; i++){
			if(max==media){
				printf("%s", med);
			}
			else if(max==0){
				printf("%s", zero);
			}
			else if(array[i]>=max){
				printf("%5c", c);	
			}
			else if(array[i<max]){
				printf("%s", space);
			}
		}
	}
	
	int q=-1;
	for(; min<=q; q--){
		printf("\n%5d", q);
		i=0;
		for(i=0; i<N; i++){
			if(q==media){
				printf("%s", med);
			}
			else if(array[i]<=q){
				printf("%5c", c);
			}
			else if(array[i]>q){
				printf("%s", space);
			}
		}
	}
	
	printf("\n%5d\n", --min);
	printf("%s", space);
	for(i=0; i<N; i++){
		printf("%5d", array[i]);
	}
	printf("\n%s", space);
	for(i=0; i<N; i++){
		printf("%5d", i);
	}
	
	return 0;
}
