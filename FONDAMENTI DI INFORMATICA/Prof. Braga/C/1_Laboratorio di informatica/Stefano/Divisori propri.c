#include <stdio.h>

int main(){
	
	int n=0;
	int k=0;
	printf("---------DIVISORI PROPRI---------\n");
	printf("Scrivi un numero: ");
	scanf("%d", &n);
	k=n/2;
	printf("I divisori propri di %d sono: ", n);
	while(k>0){
		if(n%k==0)
			printf("%d ",k);
		k--;
	}
	
	return 0;
	
}
