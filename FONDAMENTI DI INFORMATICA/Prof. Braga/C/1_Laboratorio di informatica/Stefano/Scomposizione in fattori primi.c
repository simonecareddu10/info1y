#include <stdio.h>
#include <math.h>

int main(){
	
	int num=0, nstart=0, first=0, k=0, rest=0, i=0, ipassaggio=0;
	
	printf("----Scomposizione in fattori primi----");
	printf("\nInserisci un numero da scomporre in fattor primi: ");
	scanf("%d", &num);
	printf("Numero: %d", num);
	printf("\n%d => ", num);
	nstart=num;
	
	for(first=2 ; first<nstart; first++){
		while(num%first==0){
			i++;
			num=num/first;
		}
		if(i!=0){
			if(i>1){
				printf("%d^%d ", first, i);
				i=0;
				ipassaggio=1;
			}
			else if(i==1){
				printf("%d ", first);
				i=0;
				ipassaggio=1;
			}
		}
	}
	
	if(nstart==0){
		printf("Non scomponibile");
	}
	else if(nstart==1){
		printf("E' un numero primo");
	}
	else if(nstart==2){
		printf("E' un numero primo");
	}
	else if(nstart>2 && ipassaggio==0){
		printf("E' un numero primo");
	}
	
	return 0;
}
