#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

typedef struct{
	char boggle[4][4];
} structg;

int contenuta(structg griglia, char parola2[]); //verifica se la parola � contenuta nel boggle (senza ritorni sulla stessa casella, quindi versione classica)
int contrl(char str[], FILE * file); //controlla se la parola esiste nel dizionario Italiano
void inizializza(structg *griglia); //inizializza il boggle a tutti '\0'
void inserisci(structg *griglia, char str[17]);	//inserisci una stringa nel boggle
void stampa(structg *griglia);	//stampa il boggle

int main(){
	
	int d=0;
	FILE * file;
	char parola[17]="";
	char str[17] = "ENAPMTSRDUITCONA";
	structg griglia;
	structg *p;
	p=&griglia;
	inizializza(p);
	inserisci(p, str);
	printf("---Boggle---\n");
	stampa(p);	//stampa delLa griglia
	//inizio seconda parte
	printf("Inserisci il maggior numero di parole che riesci a riconoscere\nScrivi una parola (o scrivi \"fine\" per terminare): ");
	scanf("%s", parola);
	for(; (strcmp(parola, "fine"))!=0 ;){
		if(contrl(parola, file)==-1){
			printf("Errore nell'apertura' del file \"dizionarioitaliano.txt\"");
			return 0;
		}
		if(contenuta(griglia, parola) && contrl(parola, file)){
			printf("Hai trovato una parola!\n");
			d++;
		}
		else if(contenuta(griglia, parola) && (!contrl(parola, file))){
			printf("La parola c'e' ma non esiste (nel vocabolario)\n");
		}
		else if(!contenuta(griglia, parola)){
			printf("La parola non e' contenuta nella griglia\n");
		}
		stampa(&griglia);
		printf("Scrivi un'altra parola (o scrivi \"fine\" per terminare): ");
		scanf("%s", parola);
	}
	printf("Hai trovato %d parole", d);
	
	
	return 0;
}

int contenuta(structg griglia, char parola2[]){ //verifica se la parola � contenuta nel boggle (senza ritorni sulla stessa casella, quindi versione classica)
	
	structg griglia2;
	char parola[17]="";
	int i,t, k, itemp, ttemp;
	for(i=0; i<=strlen(parola2)-1; i++){
		if(parola2[i]>='a' && parola2[i]<='z'){
			parola[i]=(parola2[i]-('a'-'A'));
		}
		else{
			parola[i]=parola2[i];
		}
	}
	parola[i]='\0';
	for(i=0; i<4; i++){	//copia del boggle originale nel boggle2 (ci servir� per ripristinarlo)
		for(t=0; t<4; t++){
			griglia2.boggle[i][t]=griglia.boggle[i][t];
		}
	}
	for(i=0; i<4; i++){
		for(t=0; t<4; t++){
			if(parola[0]==griglia.boggle[i][t]){	//ricerca casella uguale al primo carattere della stringa
				//verifica la sequenza
				griglia.boggle[i][t]='\0';
				itemp=i;
				ttemp=t;
				for(k=1; k<=strlen(parola)-1; k++){
					if((i-1>=0) && (t-1>=0) && parola[k]==griglia.boggle[i-1][t-1]){ //verifica con "ricordo" (con la tecnica del '\0') (per tutti gli if)
						griglia.boggle[i-1][t-1]='\0';
						i--;
						t--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((i-1>=0) && parola[k]==griglia.boggle[i-1][t]){
						griglia.boggle[i-1][t]='\0';
						i--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((i-1>=0) && (t+1<=3) && parola[k]==griglia.boggle[i-1][t+1]){
						griglia.boggle[i-1][t+1]='\0';
						i--;
						t++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((t-1>=0) && parola[k]==griglia.boggle[i][t-1]){
						griglia.boggle[i][t-1]='\0';
						t--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((t+1<=3) && parola[k]==griglia.boggle[i][t+1]){
						griglia.boggle[i][t+1]='\0';
						t++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((i+1<=3) && (t-1>=0) && parola[k]==griglia.boggle[i+1][t-1]){
						griglia.boggle[i+1][t-1]='\0';
						i++;
						t--;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((i+1<=3) && parola[k]==griglia.boggle[i+1][t]){
						griglia.boggle[i+1][t]='\0';
						i++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else if((i+1<=3) && (t+1<=3) && parola[k]==griglia.boggle[i+1][t+1]){
						griglia.boggle[i+1][t+1]='\0';
						i++;
						t++;
						if(k==strlen(parola)-1){
							return 1;
						}
					}
					else{
						for(i=0; i<4; i++){	//se non trova corrispondenze, esce, e il boggle1 viene ripristinato all'originale assegnandogli il boggle2
							for(t=0; t<4; t++){
								griglia.boggle[i][t]=griglia2.boggle[i][t];
							}
						}
						i=itemp;	//riport i e t (modificati nella procedura di verifica)->
						t=ttemp;	//->ai valori che avevano quando si ricercava la casella di inizio
						k=strlen(parola)-1;	//utilizzato per uscire senza usare break
					}
				}
			}
		}
	}
	return 0;
}

int contrl(char str[], FILE * file){ //controlla se la parola esiste nel dizionario Italiano
						//e restituisce -1 se ci sono errori nell'apertura del file txt
	char vett[20]="";
	if((file=fopen("italiandictionary.txt", "r"))!=NULL){
		for(;! feof(file);){
			fgets(vett, 90, file);
			vett[strlen(vett)-1]='\0';
			if(strcmp(vett, str)==0){
				fclose(file);
				return 1;
			}
		}
		fclose(file);
		return 0;
	}
	else{
		return -1;
	}
}

void inserisci(structg *griglia, char str[17]){
	int i, t, k;
	for(k=0; k<16;){
		for(i=0; i<4; i++){
			for(t=0; t<4; t++){
				(*griglia).boggle[i][t]=str[k];
				k++;
			}
		}
	}
}

void inizializza(structg *griglia){
	int i, t;
	for(i=0; i<4; i++){
		for(t=0; t<4; t++){
			(*griglia).boggle[i][t]='\0';
		}
	}
}

void stampa(structg *griglia){
	int i;
	for(i=0; i<4; i++){
		printf("-----------------\n");
		printf("| %c | %c | %c | %c |\n", (*griglia).boggle[i][0], (*griglia).boggle[i][1], (*griglia).boggle[i][2], (*griglia).boggle[i][3]);
	}
	printf("-----------------\n");
}
