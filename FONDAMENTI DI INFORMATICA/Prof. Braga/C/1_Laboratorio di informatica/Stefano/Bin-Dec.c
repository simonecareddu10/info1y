#include <stdio.h>
#include <math.h>

#define BASE 2

int main(){
	
	int i=0, dec=0, n=BASE, w=0;
	char c;
	char array[32] = {0};
	
	printf("--Conversione Bin-Dec--\n");
	printf("Inserisci un numero in base 2 (max 32 cifre): ");
	
	
	i=-1;
	c=0;
	scanf("%c", &c);
	for(; c>=48 && c<=47+(BASE);){
		i++;
		array[i]=c-48;
		scanf("%c", &c);
		w=1;
	}
	
	
	n=BASE;
	dec=0;
	while(i>=0){
			dec=dec+(array[i]*1);
		if(i>=1){
			i--;
			dec=dec+(array[i]*BASE);
			i--;
		}
		while(i>0){
			n=n*BASE;
			dec=dec+(array[i]*n);
			i--;
		}
		dec=dec-(array[i]*(n*BASE));
		i--;
	}
	
	if(w==1){
		printf("Conversione in Dec = %d", dec);
	}
	if(w==0){
		printf("Non hai inserito nessun numero");
	}
	
	return 0;
}
