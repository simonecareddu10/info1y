#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{

    FILE * fp, *fout;
    char riga [50];
    char nome [50];
    char cognome [50];

    fp = fopen("prova.txt","r");        //creare file testo prova.txt
    fout = fopen("cognomi.txt", "w");   //creare file testo cognomi.txt

    if(fp == NULL || fout == NULL)
    {
        printf("ERRORE APERTURA FILE\n");
    }

    while (fgets(riga,50,fp) != NULL)
    {
        printf("%s",riga);

        sscanf(riga,"%s%s",nome,cognome);

        fprintf(fout,"%s\n",cognome);
    
    }

    fclose(fp);
    fclose(fout);

    return 0;

}