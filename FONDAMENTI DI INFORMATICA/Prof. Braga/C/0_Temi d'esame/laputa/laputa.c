#include <stdio.h>

#define DIMX 8
#define DIMY 5

int main(int argc, char* argv [])
{
    int i,j,k;
    int max_riga,max_colonna;
    int temp_riga,temp_colonna;
    int mappa [DIMY][DIMX];
    FILE * fp;

    if (argc != 2)
    {
        printf("ERRORE\n");
        return 1;
    }

    fp = fopen(argv[1],"r");

    if (fp == NULL)
    {
        printf("FILE NON APERTO !\n");
        return 1;
    }

    for(i = 0; i < DIMY; i++)
    {
        for(j = 0; j < DIMX; j++)
        {
            fscanf(fp,"%d", &mappa[i][j]);
        }
    }

    fclose(fp);

    for(i = 0; i < DIMY; ++i)
    {
        max_riga = 0;
        for(j = 0; j < DIMX; ++j)
        {
            if(mappa[i][j] > max_riga)
            {
                max_riga = mappa[i][j];
                temp_riga = i;
                temp_colonna = j;
            }
        }

        max_colonna = 0;
        for(k = 0; k < DIMY; k++)
        {
            if(mappa[k][temp_colonna] > max_colonna)
            {
                max_colonna = mappa[k][temp_colonna];
            }
        }

        if (max_riga == max_colonna && max_riga != 0)
        {
            printf("%d e' un punto di vedetta ", max_riga);
            printf("in posizione [%d][%d]\n", temp_riga, temp_colonna);
        }

    }

}