//

#include <stdio.h>
#include <stdlib.h>

#define OFFSET 'a'-'A'

int main() {
    
    char par_1,par_2, risp;
    
    printf("Case sensitive (y) or not (n)?: ");
    scanf("%c", &risp);
    
    printf("Inserire due lettere: ");
    scanf(" %c%c", &par_1,&par_2);
    
    switch (risp) {
            
        case 'y':{
            
            //printf("\n%d\n%d\n", par_2,par_1); //check ASCII dei due char
            
            if(par_1-par_2==0)
                printf("Lettere uguali\n");
            
            else
                printf("Lettere diverse\n");
            break;
        }
            
        case 'n':{
            
            if (abs(par_1-par_2)==OFFSET || par_1-par_2==0)
                printf("Lettere uguali\n");
            
            else
                printf("Lettere diverse\n");
            break;
        }
            
    }
    
    return 0;
    
}
