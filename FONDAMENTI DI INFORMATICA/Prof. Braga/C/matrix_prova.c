#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//----------------------------GLOBAL VARIABLES-------------------------------------//
int rows = 4, cols = 4;

int random_number ();
void print_matrix(int matrix [rows][cols]);

int main(void)
{
    int matrix [rows][cols];

    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            matrix [i][j] = random_number ();
        }
    }

    print_matrix(matrix);

}

int random_number ()
{
    return (rand()%10);
}

void print_matrix (int matrix[rows][cols])
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            printf("%2d ", matrix[i][j]);
        }

        printf("\n");
    }
}
