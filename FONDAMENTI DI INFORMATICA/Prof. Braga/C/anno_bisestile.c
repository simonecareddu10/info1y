//

#include <stdio.h>

int main() {

    int year;
    
    printf("Inserire un anno e dir0' se e' bisestile: ");
    scanf("%d", &year);
    
    if((year>1582) && ((year%4==0 && year%100!=0) || (year%4==0 && year%400==00)))
        
        printf("%d e' bisestile\n", year);
    
    else
        
        printf("%d non e' bisestile\n", year);

    return 0;

}
