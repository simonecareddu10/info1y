#include <stdio.h>
#include <math.h>
#include <stdbool.h>

typedef struct
{
		
	int num;
	int den;
	int segno;
	
	
} frazione;

frazione build (int n, int d, int s)
{
	frazione f;
	frazione * fp = &f;
	
	printf("Inserisci numeratore: ");
	scanf("%d",&n);
		f.num = n;
	
	printf("Inserisci denominatore: ");
	scanf("%d",&d);
		f.den = d;
		
	printf("Inserisci segno(+ : +1, - : -1): ");	
	scanf("%d",&s);
		f.segno = s;
		
	return f;
	
}

void stampa (frazione f)
{

	switch (f.segno)
	{	
		case 1:
			break;
		case -1:
			printf("-");
			break;  
		
	}
	
	printf("%d",f.num);
	if(f.den != 1)
	{
		printf("/");
		printf("%d",f.den);
	}
	
	//printf("\n");
	
}

frazione riduci (frazione f)
{
	int mcd = 1;
	
	for(int i = 1; i <= f.num || i <= f.den; i++)
	{
		
		if (f.num%i == 0 && f.den%i == 0)
		{
			f.num /= i;
			f.den /= i;
			i = 1;
		}
		
	}
	
	return (f);
	
}

float valuta (frazione f)
{
	
	float val;
	
	switch (f.segno)
	{
	
		case 1:
			return val = ((float)f.num/(float)f.den);
			break;
		case -1:
			return val = -((float)f.num/(float)f.den);
			break;
			
		default:
			break; 
		
	}
	
}

int conf(frazione f1, frazione f2)
{
	int risp;
	
	if (valuta(f1)>valuta(f2))
	{
		return 1;	
	}
	
	if (valuta(f1)==valuta(f2))
	{
		return 0;	
	}
	
	if (valuta(f1)<valuta(f2))
	{
		return -1;	
	}
	
}

frazione num_fraz (float val)
{
	
	frazione f;
	f.num = val*(float)pow(10,7);
	f.den = (float)pow(10,7);
	if (val >= 0)
	{
		f.segno = 1;
	}
	else
	{
		f.segno = -1;	
	}
	
	return (f = riduci(f));
	
}

frazione inverti (frazione f)
{
	
	int temp;
	temp = f.num;
	f.num = f.den;
	f.den = temp;
	
	return f;

}

frazione opposto (frazione f)
{
	
	f.segno = -1;
	return f;
	
}

int mcm (int n1, int n2)
{
	
	int num = 1;
	bool check = false;
	
	for (int i = 1; i <= n1*n2 && !check; i++)
	{
		if (i%n1 == 0 && i%n2 == 0)
		{
			num = i;
			check = true;
		}
		
		
	}
	
	return num;
	
}

int check_segno (frazione f)
{
	if (f.segno == -1)
	{
		return -(f.num);
	}
	else if (f.segno == 1)
	{
		return (f.num);	
	}
	
}

frazione somma (frazione f1, frazione f2)
{
	
	frazione f3;
	
	f1.num *= (mcm(f1.den,f2.den)/f1.den);
	f2.num *= (mcm(f1.den,f2.den)/f2.den);
	
	f1.num = check_segno(f1);
	f2.num = check_segno(f2);
	
	f3.num = (f1.num+f2.num);
	f3.den = mcm(f1.den,f2.den);
	
	
	return (f3);
	
}

frazione prodotto (frazione f1, frazione f2)
{
	
	frazione f3;
	
	f1.num = check_segno(f1);
	f2.num = check_segno(f2);
	
	f3.num = (f1.num*f2.num);
	f3.den = (f1.den*f2.den);
	
	return f3;
	
}

frazione scalar (frazione f, int k)
{
	
	f.num = check_segno(f);
	
	printf("dentro funz scalar : %d\n\n",f.num);
	
	f.num *= k;
	
	if (f.num > 0)
	{
		f.segno = 1;
	}
	else if (f.num < 0)
	{
		f.segno = -1;
	}
	
	return f;
	
}

int main(void)
{
	
	frazione f1,f2,f3;
	frazione * f1_p = &f1;
	float val;
	int k;
	
	f1 = build(f1.num,f1.den,f1.segno);
	f1 = riduci(f1);
	stampa(f1);
	printf("\n");
	
	/*printf("Inserire scalare: ");
	scanf("%d", &k);
	
	printf("%d\n",k);
	
	stampa(riduci(scalar(f1,k)));
	printf("\n");*/
	
	
	
	f2 = build(f2.num,f2.den,f2.segno);
	f2 = riduci(f2);
	stampa(f2);
	printf("\n");
	
	switch (conf(f1,f2))
	{
		
		case 1:
			stampa(f1); printf(" > ");stampa(f2); printf("\n");
			break;
		case 0:
			stampa(f1); printf(" = ");stampa(f2); printf("\n");
			break;
		case -1:
			stampa(f1); printf(" < ");stampa(f2); printf("\n");
			break;
		
	}
	
	printf("SOMMA: ");stampa(somma(f1,f2));
	printf("\n");
	
	printf("PRODOTTO: ");stampa(prodotto(f1,f2));
	printf("\n");
	
	
	
	
	
	printf("Inserisci numero: ");
	scanf("%f",&val);
	f3 = num_fraz(val);
	printf("%.1f -> f3 = ",val);stampa(f3);
	printf("\n"); 
	
	printf("f3^(-1) = ");stampa(inverti(f3));
	printf("\n");
	
	printf("-f3 = ");stampa(opposto(f3));
	printf("\n");
	
}